import 'package:easy_shopping/model/home_model.dart';

class ProductByIdModel {
  ProductByIdModel({
    required this.status,
    this.message,
    required this.model,
  });
  late final bool status;
  late final Null message;
  late final ProductModel model;

  ProductByIdModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = null;
    model = ProductModel.fromJson(json['data']);
  }

  // Map<String, dynamic> toJson() {
  //   final _data = <String, dynamic>{};
  //   _data['status'] = status;
  //   _data['message'] = message;
  //   _data['data'] = model.toJson();
  //   return _data;
  // }
}
