part of 'main_cubit.dart';

@immutable
abstract class MainState {}

class MainInitial extends MainState {}

class CategoriesLoading extends MainState {}

class CategoriesLoaded extends MainState {}

class CategoriesFailedLoaded extends MainState {}

class CategoryByIdLoading extends MainState {}

class CategoryByIdLoaded extends MainState {}

class CategoryByIdFailedLoaded extends MainState {}

class MainPageLoading extends MainState {}

class MainPageLoaded extends MainState {}

class MainPageFailedLoaded extends MainState {}

class ProductLoading extends MainState {}

class ProductLoaded extends MainState {}

class ProductFailedLoaded extends MainState {}
