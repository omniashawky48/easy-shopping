import 'package:bloc/bloc.dart';
import 'package:easy_shopping/model/categories_model.dart';
import 'package:easy_shopping/model/home_model.dart';
import 'package:easy_shopping/model/product_model.dart';
import 'package:easy_shopping/presentation/views/categoryDetails.dart';
import 'package:easy_shopping/presentation/views/product_details.dart';
import 'package:easy_shopping/presentation/widgets/components.dart';
import 'package:easy_shopping/reposatories/ecomm_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'main_state.dart';

class MainCubit extends Cubit<MainState> {
  final EcommRepository ecommRepository;
  MainCubit(this.ecommRepository) : super(MainInitial());
  static MainCubit get(context) => BlocProvider.of(context);
  List<CategoryItem> categories = [];
  List<BannerModel> banners = [];
  List<ProductModel> products = [];

  categoriesApi() async {
    emit(CategoriesLoading());
    try {
      await ecommRepository.categories().then((CategoriesModel model) {
        if (model.status) {
          categories = model.data.data;
          emit(CategoriesLoaded());
        } else {
          emit(CategoriesFailedLoaded());
        }
      });
    } on Exception catch (e) {
      print(e.toString());
      emit(CategoriesFailedLoaded());
    }
  }

  categoryByIdApi(context, id, categoryName) async {
    emit(CategoryByIdLoading());
    try {
      await ecommRepository.categoryById(id).then((CategoriesModel model) {
        if (model.status) {
          pushTo(
              context,
              CategoryDetails(
                categoryItems: model.data.data,
                categoryName: categoryName,
              ));
          emit(CategoryByIdLoaded());
        } else {
          emit(CategoryByIdFailedLoaded());
        }
      });
    } on Exception catch (e) {
      print(e.toString());
      emit(CategoryByIdFailedLoaded());
    }
  }

  mainApi() async {
    emit(MainPageLoading());
    try {
      await ecommRepository.mainPageDate().then((HomeModel model) {
        if (model.status) {
          banners = model.data.banners;
          products = model.data.products;
          emit(MainPageLoaded());
        } else {
          emit(MainPageFailedLoaded());
        }
      });
    } on Exception catch (e) {
      emit(MainPageFailedLoaded());
    }
  }

  productById(context, int id) async {
    try {
      emit(ProductLoading());
      await ecommRepository.productById(id).then((ProductByIdModel model) {
        if (model.status) {
          pushTo(
              context,
              ProductDetails(
                productModel: model.model,
              ));
          emit(ProductLoaded());
        } else {
          emit(ProductFailedLoaded());
        }
      });
    } on Exception catch (e) {
      emit(ProductFailedLoaded());
    }
  }
}
