part of 'login_cubit.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoginLoading extends LoginState {}

class LoginSuccessLoaded extends LoginState {}

class LoginFailedLoaded extends LoginState {}
