import 'package:easy_shopping/model/login_model.dart';
import 'package:easy_shopping/reposatories/user_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  final UserRepository userRepository;
  LoginCubit(this.userRepository) : super(LoginInitial());
  static LoginCubit get(context) => BlocProvider.of(context);

  late LoginModel model;

  void loginApi({required String email, required String pw}) {
    emit(LoginLoading());
    try {
      userRepository.login(email, pw).then((LoginModel loginModel) {
        if (loginModel.status) {
          model = loginModel;
          emit(LoginSuccessLoaded());
        } else {
          emit(LoginFailedLoaded());
        }
      });
    } on Exception catch (e) {
      print(e.toString());
      emit(LoginFailedLoaded());
    }
  }
}
