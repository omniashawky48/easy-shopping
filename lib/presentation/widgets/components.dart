import 'package:flutter/material.dart';

void navigateTo(context, routeName) => Navigator.pushNamed(context, routeName);
void pushTo(context, widget) => Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => widget,
      ),
    );
void navigateAndFinish(
  context,
  routeName,
) =>
    Navigator.pushNamedAndRemoveUntil(
      context,
      routeName,
      (route) {
        return false;
      },
    );
