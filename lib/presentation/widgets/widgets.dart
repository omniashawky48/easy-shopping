import 'package:easy_shopping/shared/color_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget mainContainer({required Widget child, required BuildContext context}) =>
    Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            color: ColorManager.white,
            borderRadius: const BorderRadius.only(
                topRight: Radius.circular(20), topLeft: Radius.circular(20))),
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: child,
        ));

Widget mainTextField({required String label, required controller}) => TextField(
    controller: controller, decoration: InputDecoration(label: Text(label)));

Widget space20() => const SizedBox(
      height: 20,
    );
Widget space40() => const SizedBox(
      height: 40,
    );

Widget loadingWidget = const Center(child: CircularProgressIndicator());
