import 'package:easy_shopping/model/home_model.dart';
import 'package:easy_shopping/presentation/widgets/widgets.dart';
import 'package:easy_shopping/shared/color_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProductDetails extends StatelessWidget {
  final ProductModel productModel;
  const ProductDetails({Key? key, required this.productModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorManager.white,
      appBar: AppBar(
        title: Text(
          productModel.name,
          style: TextStyle(color: ColorManager.black),
        ),
        backgroundColor: ColorManager.lightWhite,
        leading: BackButton(
          color: ColorManager.black,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.network(
              productModel.image,
              height: 300,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
            Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  ),
                  color: Color(0xff000000),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      space20(),
                      Text(
                        productModel.name,
                        textAlign: TextAlign.center,
                        style:
                            TextStyle(fontSize: 16, color: ColorManager.white),
                      ),
                      space20(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'Price : ',
                            style: TextStyle(
                                fontSize: 14, color: ColorManager.white),
                          ),
                          Text(
                            productModel.price.toString(),
                            style: TextStyle(
                                fontSize: 14, color: ColorManager.white),
                          ),
                        ],
                      ),
                      space20(),
                      Text(
                        productModel.description.toString(),
                        style:
                            TextStyle(fontSize: 14, color: ColorManager.white),
                      ),
                      space20(),
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
