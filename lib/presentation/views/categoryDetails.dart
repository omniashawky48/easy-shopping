import 'package:easy_shopping/cubit/main/main_cubit.dart';
import 'package:easy_shopping/model/categories_model.dart';
import 'package:easy_shopping/shared/color_manager.dart';
import 'package:flutter/material.dart';

class CategoryDetails extends StatelessWidget {
  final List<CategoryItem> categoryItems;
  final String categoryName;
  const CategoryDetails(
      {Key? key, required this.categoryItems, required this.categoryName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorManager.white,
        appBar: AppBar(
          title: Text(
            categoryName,
            style: TextStyle(color: ColorManager.black),
          ),
          backgroundColor: ColorManager.lightWhite,
          leading: BackButton(
            color: ColorManager.black,
          ),
        ),
        body: ListView.builder(
            physics: const BouncingScrollPhysics(),
            itemCount: categoryItems.length,
            shrinkWrap: true,
            itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.all(8.0),
                child: InkWell(
                  onTap: () {
                    MainCubit.get(context)
                        .productById(context, categoryItems[index].id);
                  },
                  child: Row(
                    children: [
                      Image.network(
                        categoryItems[index].image,
                        height: 150,
                        width: 150,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(categoryItems[index].name),
                            // Text(categoryItems[index].price.toString()),
                          ],
                        ),
                      )
                    ],
                  ),
                ))));
  }
}
