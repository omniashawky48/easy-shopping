import 'package:easy_shopping/presentation/widgets/components.dart';
import 'package:easy_shopping/presentation/widgets/widgets.dart';
import 'package:easy_shopping/shared/routes_manager.dart';
import 'package:easy_shopping/shared/strings_manager.dart';
import 'package:flutter/material.dart';

class RegisterView extends StatelessWidget {
  RegisterView({Key? key}) : super(key: key);

  final emailController = TextEditingController();
  final userNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(AppStrings.register),
      ),
      body: mainContainer(
          context: context,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              space20(),
              mainTextField(
                  label: AppStrings.username, controller: userNameController),
              space20(),
              mainTextField(
                  label: AppStrings.email, controller: emailController),
              space20(),
              ElevatedButton(
                  onPressed: () {
                    navigateTo(context, Routes.mainRoute);
                  },
                  child: const Text(AppStrings.register)),
              space20(),
              TextButton(
                  onPressed: () {
                    navigateTo(context, Routes.loginRoute);
                  },
                  child: const Text(AppStrings.login)),
            ],
          )),
    );
  }
}
