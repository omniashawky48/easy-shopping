import 'package:conditional_builder_null_safety/conditional_builder_null_safety.dart';
import 'package:easy_shopping/cubit/main/main_cubit.dart';
import 'package:easy_shopping/presentation/widgets/widgets.dart';
import 'package:easy_shopping/reposatories/ecomm_repository.dart';
import 'package:easy_shopping/shared/font_manager.dart';
import 'package:easy_shopping/shared/strings_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainView extends StatelessWidget {
  final EcommRepository ecommRepository;

  const MainView({Key? key, required this.ecommRepository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(AppStrings.main),
        ),
        body: BlocBuilder<MainCubit, MainState>(
          builder: (context, state) {
            var cubit = MainCubit.get(context);
            return mainContainer(
              context: context,
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Text(
                              'Categories',
                              style: TextStyle(fontSize: FontSize.s20),
                            ),
                            ConditionalBuilder(
                                condition: cubit.categories.isNotEmpty,
                                builder: (context) => SizedBox(
                                      height: 150,
                                      child: ListView.builder(
                                        itemCount: cubit.categories.length,
                                        shrinkWrap: true,
                                        scrollDirection: Axis.horizontal,
                                        itemBuilder: (context, index) =>
                                            InkWell(
                                          onTap: () {
                                            cubit.categoryByIdApi(
                                                context,
                                                cubit.categories[index].id,
                                                cubit.categories[index].name);
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: SizedBox(
                                              height: 115,
                                              width: 100,
                                              child: Column(
                                                children: [
                                                  Image.network(
                                                    cubit.categories[index]
                                                        .image,
                                                    height: 110,
                                                  ),
                                                  Text(
                                                    cubit
                                                        .categories[index].name,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: const TextStyle(
                                                        fontSize: FontSize.s16),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                fallback: null),
                            ConditionalBuilder(
                                condition: cubit.products.isNotEmpty,
                                builder: (context) => ListView.builder(
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemCount: cubit.products.length,
                                    shrinkWrap: true,
                                    itemBuilder: (context, index) => Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: InkWell(
                                          onTap: () {
                                            cubit.productById(context,
                                                cubit.products[index].id);
                                          },
                                          child: Row(
                                            children: [
                                              Image.network(
                                                cubit.products[index].image,
                                                height: 150,
                                                width: 150,
                                              ),
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(cubit
                                                        .products[index].name),
                                                    Text(cubit
                                                        .products[index].price
                                                        .toString()),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ))),
                                fallback: null)
                          ],
                        ),
                        if (state is CategoriesLoading ||
                            state is MainPageLoading ||
                            state is ProductLoading ||
                            state is CategoryByIdLoading)
                          Container(
                              height: MediaQuery.of(context).size.height,
                              child: loadingWidget)
                      ],
                    ),
                  ],
                ),
              ),
            );
          },
        ));
  }
}
