import 'package:easy_shopping/cubit/login/login_cubit.dart';
import 'package:easy_shopping/presentation/widgets/components.dart';
import 'package:easy_shopping/presentation/widgets/widgets.dart';
import 'package:easy_shopping/reposatories/user_repository.dart';
import 'package:easy_shopping/shared/routes_manager.dart';
import 'package:easy_shopping/shared/shared_preferences.dart';
import 'package:easy_shopping/shared/strings_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginView extends StatelessWidget {
  final UserRepository userRepository;

  LoginView({Key? key, required this.userRepository}) : super(key: key);

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(AppStrings.login),
      ),
      body: mainContainer(
          context: context,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                space20(),
                mainTextField(
                    label: AppStrings.username, controller: emailController),
                space20(),
                mainTextField(
                    label: AppStrings.password, controller: passwordController),
                space20(),
                BlocProvider(
                    create: (context) => LoginCubit(userRepository),
                    child: BlocConsumer<LoginCubit, LoginState>(
                        listener: (context, state) {
                      if (state is LoginSuccessLoaded) {
                        CacheHelper.setUserToken(
                                LoginCubit.get(context).model.data.token)
                            .then((value) {
                          token = LoginCubit.get(context).model.data.token;
                          navigateAndFinish(
                            context,
                            Routes.mainRoute,
                          );
                        });
                      } else if (state is LoginFailedLoaded) {}
                    }, builder: (context, state) {
                      if (state is LoginLoading) {
                        return loadingWidget;
                      }
                      return ElevatedButton(
                          onPressed: () {
                            LoginCubit.get(context).loginApi(
                                email: emailController.text,
                                pw: passwordController.text);
                          },
                          child: const Text(AppStrings.login));
                    })),
                space20(),
                TextButton(
                    onPressed: () {
                      navigateTo(context, Routes.registerRoute);
                    },
                    child: const Text(AppStrings.register)),
              ],
            ),
          )),
    );
  }
}
