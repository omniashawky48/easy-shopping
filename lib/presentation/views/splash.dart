import 'dart:async';

import 'package:easy_shopping/presentation/widgets/components.dart';
import 'package:easy_shopping/shared/color_manager.dart';
import 'package:easy_shopping/shared/routes_manager.dart';
import 'package:easy_shopping/shared/shared_preferences.dart';
import 'package:easy_shopping/shared/strings_manager.dart';
import 'package:flutter/material.dart';

class SplashView extends StatefulWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer.periodic(const Duration(seconds: 3), (timer) {
      CacheHelper.getUserToken().then((value) {
        token = value;
        navigateTo(
            context, value.isNotEmpty ? Routes.mainRoute : Routes.loginRoute);
      });

      timer.cancel();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      width: 200,
      color: ColorManager.primary,
    );
  }
}
