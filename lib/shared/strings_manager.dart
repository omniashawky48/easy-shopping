String token = "";

class AppStrings {
  static const String noRouteFound = "no route found";
  static const String skip = "skip";
  static const String username = "User Name";
  static const String email = "Email";
  static const String password = "Password";
  static const String usernameError = "invalid user name";
  static const String passwordError = "invalid password";
  static const String forgetPassword = "forgot_password_text";
  static const String registerText = "register_text";
  static const String loading = "loading";
  static const String retry_again = "retry_again";
  static const String ok = "ok";
  static const String emailHint = 'email';
  static const String invalidEmail = "invalid email";
  static const String resetPassword = "reset password";
  static const String success = "success";
  static const String profilePicture = "upload_profile_picture";
  static const String photoGalley = "photo_gallery";
  static const String photoCamera = "camera";
  static const String register = "Register";
  static const String haveAccount = "already_have_account";
  static const String main = "Main";
  static const String login = "Login";
  static const String notifications = "notification";
  static const String search = "search";
  static const String settings = "settings";
  static const String services = "services";
  static const String stores = "stores";
  static const String details = "details";
  static const String about = "about";
  static const String storeDetails = "store_details";
  static const String changeLanguage = "change_language";
  static const String contactUs = "contact_us";
  static const String inviteYourFriends = "invite_your_friends";
  static const String logout = "logout";

  // error handler
  static const String badRequestError = "bad_request_error";
  static const String noContent = "no_content";
  static const String forbiddenError = "forbidden_error";
  static const String unauthorizedError = "unauthorized_error";
  static const String notFoundError = "not_found_error";
  static const String conflictError = "conflict_error";
  static const String internalServerError = "internal_server_error";
  static const String unknownError = "unknown_error";
  static const String timeoutError = "timeout_error";
  static const String defaultError = "default_error";
  static const String cacheError = "cache_error";
  static const String noInternetError = "no_internet_error";
}
