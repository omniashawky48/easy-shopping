import 'package:shared_preferences/shared_preferences.dart';

const String PREFS_KEY_TOKEN = "PREFS_KEY_TOKEN";

class CacheHelper {
  static late SharedPreferences sharedPreferences;

  static init() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  static Future<void> setUserToken(String token) async {
    sharedPreferences.setString(PREFS_KEY_TOKEN, token);
  }

  static Future<String> getUserToken() async {
    return sharedPreferences.getString(PREFS_KEY_TOKEN) ?? "";
  }
}
