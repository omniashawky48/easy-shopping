import 'package:easy_shopping/presentation/views/login.dart';
import 'package:easy_shopping/presentation/views/main.dart';
import 'package:easy_shopping/presentation/views/register.dart';
import 'package:easy_shopping/presentation/views/splash.dart';
import 'package:easy_shopping/reposatories/user_repository.dart';
import 'package:easy_shopping/shared/strings_manager.dart';
import 'package:flutter/material.dart';

import '../main.dart';

class Routes {
  static const String splashRoute = "/";
  static const String onBoardingRoute = "/onBoarding";
  static const String loginRoute = "/login";
  static const String registerRoute = "/register";
  static const String forgotPasswordRoute = "/forgotPassword";
  static const String mainRoute = "/main";
  static const String storeDetailsRoute = "/storeDetails";
}

class RouteGenerator {
  static UserRepository userRepository = UserRepository();

  static Route<dynamic> getRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case Routes.splashRoute:
        return MaterialPageRoute(builder: (_) => const SplashView());
      case Routes.mainRoute:
        return MaterialPageRoute(
            builder: (_) => MainView(
                  ecommRepository: MyApp.ecommRepository,
                ));
      case Routes.loginRoute:
        return MaterialPageRoute(
            builder: (_) => LoginView(
                  userRepository: userRepository,
                ));
      case Routes.registerRoute:
        return MaterialPageRoute(builder: (_) => RegisterView());
      default:
        return unDefinedRoute();
    }
  }

  static Route<dynamic> unDefinedRoute() {
    return MaterialPageRoute(
        builder: (_) => Scaffold(
              appBar: AppBar(
                title: const Text(AppStrings.noRouteFound),
              ),
              body: const Center(child: Text(AppStrings.noRouteFound)),
            ));
  }
}
