import 'package:flutter/material.dart';

class ColorManager {
  static Color primary = const Color(0xff2FDBBC);
  static Color primaryOpacity70 = const Color(0xff2FDBBC).withOpacity(.7);
  static const Color primaryLight = Color(0xff67E5CE);
  static const Color primaryDark = Color(0xff1EAE94);
  static const Color darkGrey = Color(0xff525252);
  static const Color grey = Color(0xff737477);
  static const Color lightGrey = Color(0xff737477);

  // new colors
  static Color darkPrimary = const Color(0xffd17d11);
  static Color grey1 = const Color(0xff707070);
  static Color grey2 = const Color(0xff797979);
  static Color white = const Color(0xffFFFFFF);
  static Color lightWhite = const Color(0xffF7F7F7);
  static Color error = const Color(0xffe61f34);
  static Color black = const Color(0xff000000); // red color
}
