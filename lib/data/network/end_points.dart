const LOGIN = 'login';

const REGISTER = 'register';

const HOME = 'home';

const GET_CATEGORIES = 'categories';

const CATEGORY_BY_ID = 'categories/';

const PRODUCT_BY_ID = 'products/';

const FAVORITES = 'favorites';

const PROFILE = 'profile';

const UPDATE_PROFILE = 'update-profile';

const SEARCH = 'products/search';
