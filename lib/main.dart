import 'package:easy_shopping/reposatories/ecomm_repository.dart';
import 'package:easy_shopping/shared/bloc_observer.dart';
import 'package:easy_shopping/shared/routes_manager.dart';
import 'package:easy_shopping/shared/shared_preferences.dart';
import 'package:easy_shopping/shared/theme_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'cubit/main/main_cubit.dart';
import 'data/network/dio_helper.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  DioHelper.init();
  CacheHelper.init();
  BlocOverrides.runZoned(
    () => runApp(Phoenix(child: const MyApp())),
    blocObserver: MyBlocObserver(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static EcommRepository ecommRepository = EcommRepository();

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => MainCubit(ecommRepository)
            ..categoriesApi()
            ..mainApi(),
        )
      ],
      child: MaterialApp(
        title: 'Easy Shopping',
        debugShowCheckedModeBanner: false,
        onGenerateRoute: RouteGenerator.getRoute,
        initialRoute: Routes.splashRoute,
        theme: getApplicationTheme(),
      ),
    );
  }
}
