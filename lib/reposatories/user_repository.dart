import 'package:easy_shopping/data/network/dio_helper.dart';
import 'package:easy_shopping/data/network/end_points.dart';
import 'package:easy_shopping/model/login_model.dart';

class UserRepository {
  Future<LoginModel> login(String email, String pw) async {
    final response = await DioHelper.postData(
        url: LOGIN, data: {"email": email, "password": pw});
    print(response);
    return LoginModel.fromJson(response.data);
  }

  userData() {}
}
