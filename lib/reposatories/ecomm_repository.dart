import 'package:easy_shopping/data/network/dio_helper.dart';
import 'package:easy_shopping/data/network/end_points.dart';
import 'package:easy_shopping/model/categories_model.dart';
import 'package:easy_shopping/model/home_model.dart';
import 'package:easy_shopping/model/product_model.dart';

class EcommRepository {
  Future<CategoriesModel> categories() async {
    final response = await DioHelper.getData(url: GET_CATEGORIES);
    return CategoriesModel.fromJson(response.data);
  }

  Future<CategoriesModel> categoryById(id) async {
    final response =
        await DioHelper.getData(url: CATEGORY_BY_ID + id.toString());
    return CategoriesModel.fromJson(response.data);
  }

  Future<HomeModel> mainPageDate() async {
    final response = await DioHelper.getData(url: HOME);
    return HomeModel.fromJson(response.data);
  }

  Future<ProductByIdModel> productById(id) async {
    final response =
        await DioHelper.getData(url: PRODUCT_BY_ID + id.toString());
    return ProductByIdModel.fromJson(response.data);
  }
}
